﻿param(
  [string] $Source = ".",
  [string] $Destination = ".",
  [string] $PkgsName = "busshark.dissectors",
  [string] $PkgsVersion = "1.0.0"
)

Compress-Archive "$Source\LICENSE", "$Source\VERSION", "$Source\README.asciidoc", "$Source\plugins", "$Source\profiles", "$PSScriptRoot\install.ps1", "$PSScriptRoot\uninstall.ps1", "$PSScriptRoot\install.sh", "$PSScriptRoot\uninstall.sh" "$Destination\$PkgsName-$PkgsVersion.zip" -Force
