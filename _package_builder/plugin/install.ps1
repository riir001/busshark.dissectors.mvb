﻿param(
  [string] $Source = ".",
  [string] $Destination = "$env:appdata\Wireshark",
  [bool] $Global = $false
)

switch ( $Global ) {
  $false { $Destination = $Destination }
  $true { $Destination = "$env:programfiles\Wireshark" }
  default { $Destination = $Destination }
}

# Remove old folder 
Remove-Item "$Destination\plugins\busshark.dissectors" -Recurse -Force
Remove-Item "$Destination\profiles\busshark.mvb" -Recurse -Force
Remove-Item "$Destination\profiles\busshark.wtb" -Recurse -Force

New-Item -Path "$Destination\plugins\busshark.dissectors" -Type Directory -Force
New-Item -Path "$Destination\profiles\busshark.mvb" -Type Directory -Force
New-Item -Path "$Destination\profiles\busshark.wtb" -Type Directory -Force

Copy-Item -Path "$Source\plugins\busshark.dissectors\*" -Destination "$Destination\plugins\busshark.dissectors"  -PassThru -Force -Recurse
Copy-Item -Path "$Source\profiles\busshark.mvb\*" -Destination "$Destination\profiles\busshark.mvb"  -PassThru -Force -Recurse
Copy-Item -Path "$Source\profiles\busshark.wtb\*" -Destination "$Destination\profiles\busshark.wtb"  -PassThru -Force -Recurse

Copy-Item -Path "$Source\LICENSE" -Destination "$Destination\plugins\busshark.dissectors"  -PassThru -Force -Recurse
Copy-Item -Path "$Source\LICENSE" -Destination "$Destination\profiles\busshark.mvb"  -PassThru -Force -Recurse
Copy-Item -Path "$Source\LICENSE" -Destination "$Destination\profiles\busshark.wtb"  -PassThru -Force -Recurse
