param(
  [string] $Destination = ".",
  [string] $PkgsVersion = "1.0.0"
)

pwsh "$PSScriptRoot\plugin\create_package.ps1" "$PSScriptRoot\.." $Destination -PkgsVersion $PkgsVersion
pwsh "$PSScriptRoot\docker\create_package.ps1" "$PSScriptRoot\.." $Destination -PkgsVersion $PkgsVersion
