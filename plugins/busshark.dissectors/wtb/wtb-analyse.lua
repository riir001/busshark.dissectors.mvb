local analyse = {}


function analyse.run(meta_analyse_result, config)
    local analyse_api = meta_analyse_result.analyse
    local tree = meta_analyse_result.tree

    local status_codes = config.post_status_codes().value
    local is_valid = not analyse_api.contains_errors(status_codes)

    if is_valid then
        tree:add(config.ana_protocol.fields.is_valid, true)
    end
end

return analyse
