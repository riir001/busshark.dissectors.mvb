local lu = require('luaunit')

package.path = package.path .. ';./../../plugins/busshark.dissectors/?.lua;'
local cache = require("types/chache")

function Test_chache_new()
    lu.assertNotNil(cache.new())
end

function Test_chache_update()
    local test_cache = cache.new()
    lu.assertNotNil(test_cache)

    cache.update(test_cache, 23, "foo")

    lu.assertEquals(
        test_cache["foo"],
        23
    )

    cache.update(test_cache, 42, "foo")

    lu.assertEquals(
        test_cache["foo"],
        42
    )

    cache.update(test_cache, 4711, "bar", "foo")

    lu.assertEquals(
        test_cache["bar"]["foo"],
        4711
    )

    cache.update(test_cache, "foo", 0, 0)

    lu.assertEquals(
        test_cache[0][0],
        "foo"
    )


    cache.update(test_cache, "bar", 1, 1)

    lu.assertEquals(
        test_cache[1][1],
        "bar"
    )
end

function Test_chache_get()
    local test_cache = {
        foo = 23,
        bar = { foo = 4711 },
        [0] = { [0] = "foo" },
        [1] = { [1] = "bar" },
    }
    
    lu.assertNotNil(test_cache)

    cache.update(test_cache, 23, "foo")

    lu.assertEquals(
        cache.get(test_cache, "foo"),
        23
    )
    cache.update(test_cache, 4711, "bar", "foo")

    lu.assertEquals(
        cache.get(test_cache, "bar", "foo"),
        4711
    )

    cache.update(test_cache, "foo", 0, 0)

    lu.assertEquals(
        cache.get(test_cache, 0, 0),
        "foo"
    )

    cache.update(test_cache, "bar", 1, 1)

    lu.assertEquals(
        cache.get(test_cache, 1, 1),
        "bar"
    )
end

os.exit(lu.LuaUnit.run())
