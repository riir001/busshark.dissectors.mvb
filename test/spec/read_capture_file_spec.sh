Describe 'read_capture_file.sh'
  run_read_capture_file_test() {     
    TMP_READ_CAPTURE_FILE=$(mktemp)  
    tshark -r "$1" -O "busshark,busshark.meta,mvb,mvb.mf,mvb.sf,mvb.sf.msg,bussharkprotocol_ana,wtb,busshark.wtb.data,busshark.wtb.link-header,busshark.wtb.network-header" | sed -r 's/(payload.{20}).*/\1/' | sed -r 's/(status codes:.).*//' | sed -r 's/(Ethernet II.).*//' > "${TMP_READ_CAPTURE_FILE}"
    cmp "$2" "${TMP_READ_CAPTURE_FILE}"
  }

  Parameters
    "test_mvb-line-rti-filter"
    "test_wtb1"
    "test_mvb2net-only-f12"
    "test_mvb2net-esf"
    "test_mvb-payload-version3"
    "test_wtb-payload-version3"
  End

 It "read a file $1"
    When run run_read_capture_file_test "../example-pcap-files/$1.pcapng" "resources/expected_read_$1_result"
    The status should be success
  End
End